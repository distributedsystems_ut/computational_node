'''
Created on Sep 5, 2016

@author: devel
'''
import math
from socket import AF_INET, SOCK_STREAM, socket
import requests
import json
import ast
import threading
import time
import os

class ComputationalNode:
    def __init__(self,port,encoder,buff_size):
        self.buffer_size = buff_size
        self.port = port
        self.encoder = encoder
        self.threads = list()


    def loop(self):
        s = socket(AF_INET, SOCK_STREAM)
        print ('TCP Socket created')
        # Binding the TCP/IP socket to loop-back address and port 7777
        s.bind(('0.0.0.0',self.port))
        print ("socket bound to {}".format(s.getsockname()))

        # Put socket into listening state
        backlog = 5 # Waiting queue size, 0 means no queue



        s.listen(backlog)

        
        while (1):
            client_socket, client_addr = s.accept()
            t = threading.Thread(target=self.UserConnection,args=(client_socket,client_addr))
            self.threads.append(t)
            t.start()
            print("----------{}----------".format(self.threads.__len__()))
        

    def UserConnection(self,cli_sock,cli_add):
        client_socket = cli_sock
        client_addr = cli_add
        current_user_id = ""
        print('New client connected from:', client_addr[0])
        print('Local end-point socket bound on:', client_socket.getsockname())
        while True :
            try :
                recv_buffer_length = self.buffer_size
                message = client_socket.recv(recv_buffer_length)
                new_message = message.decode(self.encoder)#"utf-8")# the message should contain:

                if new_message == "":
                    print("disconnected")
                    data = dict()
                    data["command"] = "delete"
                    data["id"] = current_user_id
                    json_data = json.dumps(data)
                    print("**********************")
                    print(json_data)
                    print("@@@@@@@@@@@@@@@@@@@@")
                    res = ast.literal_eval(str(POST("http://127.0.0.1:8000/api/user/",data)))
                    print(res)
                    break


                api_v, func = str(new_message).split("/")
                print(api_v)
                print('Received message:', new_message)
                    
                # if we enter here it means the protocol is 
                # correct and the message is received now DO something
                if api_v in list_of_protocols :
                    if api_v == "1":
                            
                        ##### request: 1/45|n:10011
                        ##### response: 45|isoaf31sdasw21
                        request_id,request = func.split("|")
                        if request[0] == "n": # n stands for new user
                            # first time in this database
                            param = request.split(":")[1]
                            
                            data = dict()
                            data["command"] = "new"
                            data["param"] = param
                            data["ip"] = client_addr[0]
                            json_data = json.dumps(data)
                            print("**********************")
                            print(json_data)
                            print("@@@@@@@@@@@@@@@@@@@@")
                            res = ast.literal_eval(str(POST("http://127.0.0.1:8000/api/user/",data)))
                            print(res)
                            print(request_id)
                            current_user_id = res["device_id"]
                            client_socket.sendall("{}|n:{}".format(request_id,res["device_id"]))
                            print("command received successfully ")

                        ### request: 1/74|u:isoaf31sdasw21@45.00432,43.02321
                        ### response (if no matches): 74|m:
                        ### response (if several matches): 74|m:idoadiasds,adafaosfoadf,fefriih876g

                        elif request[0] == "u" :
                            print("44444444444444444444444444444444")
                            print(request[1])
                            ip_loc = request.split(":")[1]
                            user_id , loc = ip_loc.split("@")
                            x,y = loc.split(",")
                            msg = GET("http://127.0.0.1:8000/api/user/?data="+"{}|{}".format(user_id.__str__(),"id"))

                            current_user_id = user_id

                            db_loc = msg["location"]
                            location_null_param = False
                            if db_loc == "null":
                                location_null_param = True
                            if location_null_param == True:
                                data = dict()
                                data["location"] = loc
                                data["id"] = user_id#client_addr[0]
                                data["command"] = "update"
                                json_data = json.dumps(data)
                                print(json_data)
                                res = POST("http://127.0.0.1:8000/api/user/",data)
                                print(res)
                                db_loc = "{},{}".format(x,y)


                            x0,y0 = db_loc.split(",")
                            if location_null_param or DistanceMeasure(x0,y0,x,y) >= minimum_sig_move:#significant move
                                data = dict()
                                data["command"] = "update"
                                data["location"] = loc
                                data["id"] = user_id#client_addr[0]
                                data["last_updates"] = time.time()
                                json_data = json.dumps(data)
                                print(json_data)
                                print(json_data)
                                print("@@@@@@@@@@@@@@@@@@@@")
                                res = ast.literal_eval(str(POST("http://127.0.0.1:8000/api/user/",data)))
                                print("**********************")
                                print(res)
                                print("**********************")
                                data["param"] = res["love_param"]
                                all_user = GET("http://127.0.0.1:8000/api/user_all/")
                                print("---------------------------")



                                matched_users = list()
                                for user in all_user :
                                            
                                    if MatchFinding(user["love_param"],data["param"]) and user["device_id"]!=data["id"]:
                                                
                                        matched_users.append(user["device_id"])
                                                
                                        print("found a match")
                                        print(user["device_id"])
                                            
                                print("---------------------------")

                                reuslt_string = ""
                                for item in matched_users :
                                    reuslt_string += item.__str__()+","
                                if len(reuslt_string)>0:
                                    reuslt_string = reuslt_string[0:-1]

                                client_socket.sendall("{}|m:{}".format(request_id,reuslt_string))
                            else: 
                                print("not significant1/74|u:45.00432,43.02321")
                                client_socket.sendall("{}|m:".format(request_id))
                                
                            # else:
                            #     print("location null situation")
                            #     current_user_id = user_id
                            #     data = dict()
                            #     data["location"] = loc
                            #     data["id"] = user_id#client_addr[0]
                            #     data["command"] = "update"
                            #     json_data = json.dumps(data)
                            #     print(json_data)
                            #     res = POST("http://127.0.0.1:8000/api/user/",data)
                            #     print(res)
                            #     client_socket.sendall("{}|m:".format(request_id))

                        
                        elif request[0] == "p" :
                            client_socket.sendall("{}|p".format(request_id))

                        elif request[0] == "i" :
                            current_user_id = request.split(":")[1]
                            msg = GET("http://127.0.0.1:8000/api/user/?data="+"{}|{}".format(current_user_id,"id"))
                            print("9999999999")
                            if "ip" in msg.keys() == False:
                                client_socket.sendall("{}|i:404404404404".format(request_id))
                            else :
                                print(msg)
                                user_ip = msg["ip"]
                                client_socket.sendall("{}|i:{}".format(request_id,user_ip))
            except : 
                client_socket.close()
                break
        client_socket.close()
                    


def DistanceMeasure(x1,y1,x2,y2):
    return math.sqrt((float(x1)-float(x2))**2+(float(y1)-float(y2))**2)



def GET(url):
    # api-endpoint 
    URL = url
    
    
    # defining a params dict for the parameters to be sent to the API 
    #PARAMS = {'content-type':'application/json'} 
    
    # sending get request and saving the response as response object 
    r = requests.get(url = URL) 
    
    # extracting data in json format 
    data = r.json()
    return data

def POST(url,msg):
    # defining the api-endpoint  
    API_ENDPOINT = url
    
    PARAMS = {'content-type':'application/json'} 
    # data to be sent to api 
     
    
    # sending post request and saving response as response object 
    r = requests.post(url = API_ENDPOINT, data = msg)
    
    # extracting response text 
    resp = r.text 
    return resp

def MatchFinding(str1,str2):
    if str1[0] == str2[1] and str1[1]==str2[0] and str1[2] == str2[2]:
        return True
    else :
        return False



###################### list of CONSTANTS 
list_of_protocols = ["1","2","3"]
'''
location update protocol is "1"
'''
minimum_sig_move = 0 # this can change anytime

########################################


def RunComputationalNode(port,str_format,str_len):
    c = ComputationalNode(port,str_format,str_len)
    c.loop()


if __name__ == '__main__':
    print ('Application started')

    GET("http://127.0.0.1:8000/api/clear_db")
    print("database has been cleared !")

    os.system("fuser -k 7878/tcp")
    os.system("fuser -k 7879/tcp")
    os.system("fuser -k 7880/tcp")

    computational_nodes = []

    c1 = threading.Thread(target=RunComputationalNode,args=(7878,"utf-8",1024)) 
    c1.start()

    c2 = threading.Thread(target=RunComputationalNode,args=(7879,"utf-8",1024)) 
    c2.start()

    c3 = threading.Thread(target=RunComputationalNode,args=(7880,"utf-8",1024)) 
    c3.start()